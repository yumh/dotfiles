# Dependencies

## System:

 - git
 - systemd
 - zsh
 - ag (by fzf on vim)

## Font:

- ttf-overpass
- ttf-dejavu


## Apps:

 - rager
 - mdp
 - ncmpcpp
 - conky
 - termite
 - zsh
 - i3 gaps
 - i3 blocks
 - compton
 - par
 - tmux
 - rofi


## Play nicely with:

 - neofetch
 - aspell (en + it)
 - st
