# yet another dotfiles repo

Here's my dotfiles!

They are divided into subdir in a *module* like fashon. In each
directory there is a `link.sh` executable to link the files in the
proper directory. Maybe it isn't the better way to manage those files
but for now, to me, it's okay.

I'm using different branches for different PC/laptop/OSes. As of now,
the `master` branch holds the config of my workstation and the
`mini-laptop` holds the config for a tablet of mine.

# What's inside?

### editors

configuration for my editors:

 - vimrc: a 100% standalone vim configuration
 - nvim: the home for my neovim config
 - [kakouine](https://github.com/mawww/kakoune)

### scripts

various utility script, mostly in sh

### systemd

some unit files

### various stuff

miscellaneous: tmux & htop at the time of writing

### Xorg

grafical stuff, in short:

- awesome3.5 (I'm not using it anymore)
- xmonad (+ polybar)
- i3gaps config (+ i3blocks config & related script)
- redshift config
- .x(initrc|profile|resources)
- conky

### shell

configuration file for some shell. I prefer zsh, but sometimes I use tcsh or
bash

### dependencies

check [the dependencies list](dependencies.md)

# Screenshots

Here's some screenshot. Sometime I'll to keep 'em updated

![An empty workspace with xmonad prompt open and conky](https://raw.githubusercontent.com/omar-polo/dotfiles/master/screenshots/blank-with-prompt-and-conky.png)
> An empty workspace with xmonad prompt open and conky

---

![First monitor](screenshots/first-monitor.png)
> My first monitor

---

![Working](screenshots/working.png)
> Working (emacs)

---

![Music workspace](screenshots/music.png)
> Music workspace (mpd + ncmpcpp + C.A.V.A)


# TODO:

- [ ] fix colors in xmonad to match the new colorscheme
- [ ] two monitor screenshot

