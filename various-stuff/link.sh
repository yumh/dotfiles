#!/bin/sh

mkdir -p ~/.config/htop
mkdir -p ~/.newsbeuter

ln -sv `pwd`/htoprc ~/.config/htop/htoprc
ln -sv `pwd`/tmux.conf ~/.tmux.conf
ln -sv `pwd`/digrc ~/.digrc
ln -sv `pwd`/alacritty.yml ~/.config/alacritty.yml
ln -sv `pwd`/newsbeuter-urls ~/.newsbeuter/urls
ln -sv `pwd`/nethackrc ~/.nethackrc
ln -sv `pwd`/unnethackrc ~/.unnethackrc
