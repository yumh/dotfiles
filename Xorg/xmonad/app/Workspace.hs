module Workspace where

import XMonad
import XMonad.Hooks.WallpaperSetter

workspaces :: [String]
workspaces = ["main", "web", "im", "file", "music", "reading", "video"]

  -- look for wallpaper in ~/.wallpaper/WORKSPACEID.jpg
wpSet :: X()
wpSet = wallpaperSetter defWallpaperConf
