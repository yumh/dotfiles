module Scratchpad where

import           XMonad.StackSet as W
import           XMonad.ManageHook
import           XMonad.Util.NamedScratchpad

scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "terminal"     te                ft               mt
  , NS "note"         spawnNote         findNote         manageNote
  , NS "transmission" spawnTransmission findTransmission manageTransmission
  , NS "keepassx2"    spawnKeepassx2    findKeepassx2    manageKeepassx2
  , NS "arandr"       spawnArandr       findArandr       manageArandr
  ]
  where
    te = "term -name scratchpad -bg white"
    ft = resource =? "scratchpad"
    mt = customFloating $ W.RationalRect l t w' h'
      where
        l = 1/32
        t = 1/42
        w' = 30/32
        h' = 1/3

    spawnNote  = "term -name notes -T notes -e emacsclient -t ~/notes.org"
    findNote   = resource =? "notes"
    manageNote = customFloating $ W.RationalRect l t w' h'
      where
        h' = 1/2
        w' = 1/2
        t  = 1/4
        l  = 1/4

    spawnTransmission    = "transmission-gtk"
    findTransmission     = title =? "Transmission"
    manageTransmission   = customFloating $ W.RationalRect l t w h
      where
        t = (1 - h)/2
        l = (1 - w)/2

    spawnKeepassx2    = "keepassx2"
    findKeepassx2     = className =? "Keepassx2"
    manageKeepassx2   = customFloating $ W.RationalRect l t w h
      where
        t = (1 - h)/2
        l = (1 - w)/2

    spawnArandr    = "arandr"
    findArandr     = className =? "Arandr"
    manageArandr   = customFloating $ W.RationalRect l t w' h'
      where
        h' = 0.3
        w' = 0.3
        t = (1 - h')/2
        l = (1 - w')/2

    h = 0.7
    w = 0.7

manageHook = namedScratchpadManageHook scratchpads
