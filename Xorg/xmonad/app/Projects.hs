module Projects where

import           XMonad
import           XMonad.Actions.DynamicProjects

projects :: [Project]
projects =
  [ Project { projectName      = "gimp"
            , projectDirectory = "~/Immagini"
            , projectStartHook = Just $ spawn "gimp"
            }

  , Project { projectName      = "inkscape"
            , projectDirectory = "~/Immagini"
            , projectStartHook = Just $ spawn "inkscape"
            }

  , Project { projectName      = "main"
            , projectDirectory = "~"
            , projectStartHook = Just $ do spawn "emacs"
                                           spawn "term"
            }

  , Project { projectName      = "im"
            , projectDirectory = "~"
            , projectStartHook = Just $ do spawn "telegram-desktop"
                                           spawn "term -e toxic"
            }

  , Project { projectName      = "web"
            , projectDirectory = "~"
            , projectStartHook = Just $ spawn "firefox -p foo"
            }

  , Project { projectName      = "video"
            , projectDirectory = "~/Video"
            , projectStartHook = Just $ spawn "term -e ranger"
            }

  , Project { projectName      = "xmonad"
            , projectDirectory = "~/.xmonad"
            , projectStartHook = Just $ do spawn "emacs ."
                                           spawn "term"
                                           spawn "term"
            }
  ]
