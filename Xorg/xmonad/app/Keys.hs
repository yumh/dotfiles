{-# LANGUAGE NoMonomorphismRestriction #-}

module Keys where

import           Data.Bits
import qualified Data.Map                          as M
import           Graphics.X11.ExtraTypes.XF86
import qualified System.Exit                       as E
import           XMonad
import           XMonad.Actions.CycleWS
import           XMonad.Actions.DynamicProjects
import           XMonad.Actions.GridSelect
import qualified XMonad.Actions.Submap             as SM
import           XMonad.Hooks.ManageDocks
import           XMonad.Layout.Gaps
import           XMonad.Layout.Maximize
import           XMonad.Layout.ResizableTile
import           XMonad.Layout.Spacing
import           XMonad.Layout.SubLayouts
import           XMonad.Layout.WindowNavigation
import           XMonad.Prompt.Shell
import qualified XMonad.StackSet                   as W
import           XMonad.Util.NamedScratchpad

import qualified Scratchpad  as S
import qualified Prompt      as P
import qualified Layout.Hide as H

modifier :: KeyMask
modifier = mod4Mask

navigation :: TwoD a (Maybe a)
navigation = makeXEventhandler $ shadowWithKeymap navkeys defaultHandler
  where navkeys = M.fromList [ ((0, xK_Escape), cancel) -- TODO: update the bindings and use EZConfig!
                             , ((0, xK_Return), select)
                             , ((0, xK_slash),  substringSearch navigation)

                             , ((0, xK_Left),   move (-1, 0) >> navigation)
                             , ((0, xK_h),      move (-1, 0) >> navigation)

                             , ((0, xK_Down),   move (0, 1) >> navigation)
                             , ((0, xK_j),      move (0, 1) >> navigation)

                             , ((0, xK_Up),     move (0, -1) >> navigation)
                             , ((0, xK_k),      move (0, -1) >> navigation)

                             , ((0, xK_Right),  move (1, 0) >> navigation)
                             , ((0, xK_l),      move (1, 0) >> navigation)

                             , ((0, xK_space),  setPos (0, 0) >> navigation)
                             ]
        defaultHandler = const navigation

toggleSpacing :: Int -> Int
toggleSpacing 0 = 4
toggleSpacing _ = 0

ks conf@(XConfig {XMonad.modMask = modm}) =
  [ ((modm, xK_Return), spawn "term")
  , ((modm, xK_space), sendMessage NextLayout)
  , ((modm, xK_d), spawn "rofi -show drun")
  , ((modm, xK_q), SM.submap . M.fromList $
      [ ((0, xK_Escape), io E.exitSuccess) ])

  -- navigation
  , ((modm, xK_h), sendMessage $ Go L)
  , ((modm, xK_j), sendMessage $ Go D)
  , ((modm, xK_k), sendMessage $ Go U)
  , ((modm, xK_l), sendMessage $ Go R)

  -- general window management
  , ((modm, xK_x), kill)
  , ((modm, xK_t), withFocused $ windows . W.sink) -- back to floating
  , ((modm, xK_f), withFocused $ sendMessage . maximizeRestore)
  , ((modm .|. shiftMask, xK_f), sendMessage H.HideRestore)
  , ((modm, xK_period), sendMessage $ IncMasterN (-1))
  , ((modm, xK_comma), sendMessage $ IncMasterN 1)
  , ((modm, xK_w), goToSelected def { gs_cellheight = 60
                                    , gs_cellwidth = 200
                                    , gs_navigate = navigation
                                    })
  , ((modm .|. controlMask, xK_period), onGroup W.focusDown')
  , ((modm .|. controlMask, xK_period), onGroup W.focusUp')
  , ((mod1Mask .|. controlMask, xK_period), onGroup W.focusDown')
  , ((mod1Mask .|. controlMask, xK_period), onGroup W.focusUp')

  -- managing gaps
  , ((modm, xK_g), SM.submap . M.fromList $
      [ ((0, xK_t), sendMessage ToggleGaps)
      , ((0, xK_s), sendMessage $ ModifySpacing toggleSpacing)
      , ((0, xK_a), sequence_ [ sendMessage ToggleGaps
                              , sendMessage $ ModifySpacing toggleSpacing])
      , ((0, xK_b), sendMessage ToggleStruts) ])

  -- jump to last workspace (excluding NSP)
  , ((modm, xK_Tab), toggleWS' ["NSP"])
  , ((mod1Mask, xK_Tab), toggleWS' ["NSP"])

  -- moving through workspaces [TODO]
  , ((modm .|. shiftMask, xK_m), SM.submap . M.fromList $
    [ ((0, xK_h), return ())
    , ((0, xK_j), return ())
    , ((0, xK_k), return ())
    , ((0, xK_l), return ()) ])

  -- prompts
  , ((modm, xK_p), SM.submap . M.fromList $
      [ ((0, xK_p), switchProjectPrompt P.theme)
      , ((0, xK_m), shiftToProjectPrompt P.theme)
      , ((0, xK_d), changeProjectDirPrompt P.theme)
      , ((0, xK_r), shellPrompt P.theme)
      ])

  -- scratchpads
  , ((modm, xK_s), SM.submap . M.fromList $
      [ ((0, xK_t), namedScratchpadAction S.scratchpads "terminal")
      , ((0, xK_Print), namedScratchpadAction S.scratchpads "arandr")
      , ((0, xK_n), namedScratchpadAction S.scratchpads "note")
      , ((0, xK_p), namedScratchpadAction S.scratchpads "keepassx2")
      , ((0, xK_s), namedScratchpadAction S.scratchpads "transmission")
      ])

  -- multimedia keys
  , ((0, xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume 0 -5%")
  , ((0, xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume 0 +5%")
  , ((0, xF86XK_AudioMute), spawn "pactl set-sink-mute 0 toggle")
  , ((0, xF86XK_AudioPlay), spawn "mpc toggle")
  , ((modm, xF86XK_AudioRaiseVolume), spawn "mpc next")
  , ((modm, xF86XK_AudioLowerVolume), spawn "mpc prev")
  , ((0, xF86XK_AudioNext), spawn "mpc next")
  , ((0, xF86XK_AudioPrev), spawn "mpc prev")

  -- screen shots
  , ((0, xK_Print), spawn "screenshot")
  , ((0 .|. shiftMask, xK_Print), spawn "screenshot s")
  ]
  ++

  -- NOTE: I'm using a modified version of the Dvorak programmer
  -- layout, here's why this strange stuff:
  [((m .|. modm, k), windows $ f i)
  | (i, k) <- zip (XMonad.workspaces conf) [0x26, 0x5b, 0x7b, 0x7d, 0x28, 0x3d, 0x2a, 0x29, 0x2b, 0x5d]
  , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

navigationMode conf@(XConfig {XMonad.modMask = modm}) =
  [ ((m `xor` modm, k), a >> (SM.submap . M.fromList $ navigationMode conf)) | ((m, k), a) <-
      [ ((modm, xK_h), sendMessage $ Go L)
      , ((modm, xK_j), sendMessage $ Go D)
      , ((modm, xK_k), sendMessage $ Go U)
      , ((modm, xK_l), sendMessage $ Go R)

      , ((modm .|. shiftMask, xK_h), sendMessage $ Swap L)
      , ((modm .|. shiftMask, xK_j), sendMessage $ Swap D)
      , ((modm .|. shiftMask, xK_k), sendMessage $ Swap U)
      , ((modm .|. shiftMask, xK_l), sendMessage $ Swap R)

      , ((modm .|. controlMask, xK_h), sendMessage Shrink)
      , ((modm .|. controlMask, xK_j), sendMessage MirrorShrink)
      , ((modm .|. controlMask, xK_k), sendMessage MirrorExpand)
      , ((modm .|. controlMask, xK_l), sendMessage Expand)

      , ((modm .|. shiftMask .|. controlMask, xK_h), sendMessage $ pullGroup L)
      , ((modm .|. shiftMask .|. controlMask, xK_j), sendMessage $ pullGroup D)
      , ((modm .|. shiftMask .|. controlMask, xK_k), sendMessage $ pullGroup U)
      , ((modm .|. shiftMask .|. controlMask, xK_l), sendMessage $ pullGroup R)

      , ((modm, xK_u), withFocused $ sendMessage . UnMerge)
      ]]

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  ((modm, xK_n), SM.submap . M.fromList $ (navigationMode conf)) : (ks conf)
