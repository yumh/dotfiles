{-# LANGUAGE DeriveDataTypeable, FlexibleContexts, FlexibleInstances, MultiParamTypeClasses, TypeSynonymInstances #-}
-----------------------------------------------------------------------------
-- |
-- Module      : Layout.Hide
-- Copyright   : (c) 2018 Omar Polo
-- License     : BSD3-style
--
-- Maintainer  : omar.polo@protonmail.com
-- Stability   : unstable
-- Portability : unportable
--
-- Temporary hides all the windows
--
-----------------------------------------------------------------------------

module Layout.Hide (
  -- * Usage
  -- $usage
  Hide(..),
  HideRestore(..),
  hide,
  ) where

-- $usage
-- You can use this module with the following in your @~\/.xmonad\/xmonad.hs@:
--
-- > import Layout.Hide
--
-- Then edit your @layoutHook@ by adding the Hide layout modifier:
--
-- > myLayout = hide (Full ||| etc...)
-- > main = xmonad def { layoutHook = myLayout }
--
-- For more detailed instructions on editing the layoutHook see:
--
-- "XMonad.Doc.Extending#Editing_the_layout_hook"
--
-- In the key-bindings, do something like:
--
-- >        , ((modm, xK_backslash), withFocused (sendMessage . hideRestore))
-- >        ...
--
-- 

import           XMonad hiding (Hide, hide)
import           XMonad.Layout.LayoutModifier

data Hide a = Hide Bool deriving (Read, Show)

hide :: LayoutClass l Window => l Window -> ModifiedLayout Hide l Window
hide = ModifiedLayout $ Hide False

data HideRestore = HideRestore deriving (Typeable, Eq)
instance Message HideRestore

instance LayoutModifier Hide Window where
  modifierDescription (Hide b) = if b then "Hidden" else ""

  pureModifier (Hide b) _ _ wrs =
    if b
    then ([], Just (Hide b))
    else (wrs, Nothing)

  pureMess (Hide b) m = case fromMessage m of
    Just HideRestore -> Just $ Hide (not b)
    _                -> Nothing
