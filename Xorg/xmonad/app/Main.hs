module Main where

import           Control.Applicative ((<$>))
import           XMonad
import           XMonad.Actions.DynamicProjects
import           XMonad.Actions.UpdatePointer
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.SetWMName (setWMName)
import           XMonad.Hooks.UrgencyHook
import           XMonad.Util.Run (spawnPipe, safeSpawn)
import           XMonad.Util.NamedWindows (getName)
import qualified XMonad.StackSet as SS

import qualified Dzen2 as D
import qualified Keys as K
import qualified Layouts as L
import qualified ManageWindow as M
import qualified Projects as P
import qualified Scratchpad as S
import qualified Workspace as W

data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Show, Read)

instance UrgencyHook LibNotifyUrgencyHook where
  urgencyHook LibNotifyUrgencyHook w = do
    name     <- getName w
    Just idx <- SS.findTag w <$> gets windowset
    safeSpawn "notify-send" [show name, "Activity on " ++ idx]


main :: IO ()
main = do
  left  <- spawnPipe D.bar
  _ <- spawnPipe D.info

  xmonad
    $ withUrgencyHook LibNotifyUrgencyHook
    $ dynamicProjects P.projects
    $ docks def
    { keys = K.myKeys
    , modMask = K.modifier
    , logHook = D.logHook "/home/omar" left <+> W.wpSet >> updatePointer (0.5, 0.5) (0, 0)
    , manageHook = M.manageWindow <+> manageDocks <+> S.manageHook
    , workspaces = W.workspaces
    , layoutHook = L.layouts
    , borderWidth = 2
    , normalBorderColor = "#000000"
    , focusedBorderColor = "#c03838"
    , startupHook = setWMName "LG3D"
    }
