module Prompt where

import qualified Data.List as L
import           XMonad.Prompt

theme :: XPConfig
theme = def
  { font = "xft:DejaVu Sans:pixelsize=13:antialias=true"
  , position = Top
  , bgColor = "black"
  , promptBorderWidth = 0
  , height = 20
  , historyFilter = L.nub
  , historySize = 30
  }
