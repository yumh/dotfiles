module Dzen2 where

import Data.List.Utils (replace)
import System.IO
import XMonad
import XMonad.Hooks.DynamicLog

logOut :: Handle -> String -> IO()
logOut h y@('^':_) = hPutStrLn h y   -- the first workspace is the current one, no space before its name
logOut h s = hPutStrLn h (" " ++ s)

logHook :: String -> Handle -> X()
logHook p h = dynamicLogWithPP def
              { ppHiddenNoWindows = const ""
              , ppHidden          = \ws -> if ws == "NSP" then "" else " " ++ ws ++ " "
              , ppCurrent         = g "#ad5f5f"
              , ppVisible         = g "#6a4545"
              , ppSep             = "^p(30)"
              , ppWsSep           = ""
              , ppOutput          = logOut h
              , ppTitle           = \t -> case t of
                                            "" -> ""
                                            _  -> "^i(" ++ p ++ "/.config/dzen2/haskell.xbm) " ++ t
              , ppUrgent          = dzenColor "#d63434" "black"
              , ppLayout          = \l -> case f l of
                                            "Spiral"                  -> "@"
                                            "ResizableTall"           -> "^i(" ++ p ++ "/.config/dzen2/tall.xbm)"
                                            "Mirror ResizableTall"    -> "^i(" ++ p ++ "/.config/dzen2/mtall.xbm)"
                                            "Full"                    -> "^i(" ++ p ++ "/.config/dzen2/full.xbm)"
                                            "ThreeCol"                -> "^i(" ++ p ++ "/.config/dzen2/col.xbm)"
                                            "Simplest"                -> "_" -- "Tabbed Simplest"
                                            "Magnifier ResizableTall" -> "*"
                                            _                         -> l
              }
  where f x =   replace "Tabbed "    ""
              $ replace "Spacing 4 " ""
              $ replace "Spacing 0 " ""
              $ replace "Hidden "    ""
              $ replace "Maximize "  ""
              $ replace "ReflectX "  "" x
        g c x = "^bg(" ++ c ++ ") " ++ x ++ " ^bg()"


type XftFont = String

font :: XftFont
font = "'xft:DejaVu Sans Mono:Book:pixelsize=11:antialias=true'"

dCom :: String
dCom = "-e 'sigusr1=togglehide;sigusr2=unhide'"

bar :: String
bar = "dzen2 -dock -x '60' -y '0' -h '24' -w '640' -ta 'l' -fg '#FFFFFF' -bg '#1B1D1E' -xs 1 -fn " ++ font ++ " " ++ dCom

info :: String
info =  "conky -c ~/.config/conky_dzen | dzen2 -dock -x '700' -w '1160' -h '24' -ta 'r' -bg '#1B1D1E' -fg '#FFFFFF' -y '0' -xs 1 -fn " ++ font ++ " " ++ dCom
