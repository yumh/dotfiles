{-# LANGUAGE FlexibleContexts #-}

module Layouts where

import           XMonad
import           XMonad.Hooks.ManageDocks
import           XMonad.Layout.NoBorders
import           XMonad.Layout.Gaps
import           XMonad.Layout.Maximize
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.Reflect
import           XMonad.Layout.ResizableTile
import           XMonad.Layout.Simplest
import           XMonad.Layout.Spacing
import           XMonad.Layout.SubLayouts
import           XMonad.Layout.Tabbed
import           XMonad.Layout.ThreeColumns
import           XMonad.Layout.WindowNavigation

import qualified Layout.Hide as H

tabConfig :: Theme
tabConfig = def { activeBorderColor   = "#000000"
                , activeTextColor     = "#bd93f9"
                , activeColor         = "#44475a"
                , inactiveBorderColor = "#000000"
                , inactiveTextColor   = "#6272a4"
                , inactiveColor       = "#000000"
                , fontName            = "xft:DejaVu Sans:size=9"
                }

subtab x = addTabs shrinkText tabConfig $ subLayout [] Simplest x

layouts = H.hide $ maximizeWithPadding 0 $ myGaps $ subtab $ windowNavigation $ mkToggle (single REFLECTX) smartMainLayout
  where strutsAndSpacing l = avoidStruts $ spacing 4 l
        generalLayout = ResizableTall 1 (3/100) (1/2) []
                        ||| Mirror (ResizableTall 1 (3/100) (1/2) [])
                        ||| ThreeColMid 1 (3/100) (1/2)
                        ||| Full
        mainLayout = strutsAndSpacing generalLayout
        smartMainLayout = smartBorders mainLayout
        -- myGaps = gaps [(x, y) | x <- [U, D, L, R], y <- [15]]
        myGaps = gaps [(x, y) | x <- [U, D, L, R], y <- [15]]
