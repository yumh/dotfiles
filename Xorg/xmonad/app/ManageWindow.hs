module ManageWindow where

import           Data.Monoid (Endo)
import           XMonad
import           XMonad.Hooks.ManageHelpers
import qualified XMonad.StackSet                   as W

manageWindow :: Query (Endo WindowSet)
manageWindow = composeAll
  [ className =? "Gimp" --> doShift "gimp"
  , (className =? "Gimp" <&&> fmap ("gimp-image-window" /=) role) --> doFloat

  -- the same but for gimp-2.8
  , className =? "Gimp-2.0" --> doShift "gimp"
  , (className =? "Gimp-2.0" <&&> fmap ("gimp-image-window" /=) role) --> doFloat

  , className =? "Inkscape" --> doShift "inksacape"

  , className =? "Firefox" --> doShift "web"

  , isFullscreen --> doF W.focusDown <+> doFullFloat
  ]
  where role = stringProperty "WM_WINDOW_ROLE"
