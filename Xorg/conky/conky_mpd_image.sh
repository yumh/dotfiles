#!/bin/sh

if mpc status | grep playing 2>&1 > /dev/null
then
    echo '${image ~/.config/conky/imgs/ic_pause_white_24dp_2x.png -p 55,320}'
else
    echo '${image ~/.config/conky/imgs/ic_play_arrow_white_24dp_2x.png -p 55,320}'
fi
