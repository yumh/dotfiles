-- Define your visual elements here
-- For examples, and a complete list on the possible elements and their 
-- properties, go to https://github.com/fisadev/conky-draw/
-- (and be sure to use the lastest version)

elements = {
    {
        kind = 'ring_graph',
        conky_value = 'execi 1 volume',
        center = {x = 210, y = 250},
        radius = 100,

        background_color = 0xFFFFFF,
        background_alpha = 0.5,
        background_thickness = 1,

        bar_color = 0xFFFFFF,
        bar_alpha = 0.7,
        bar_thickness = 5,

        start_angle = 140,
        end_angle = 300,
    },

    {
        kind = 'ring_graph',
        conky_value = 'execi 10 battery',
        center = {x = 210, y = 250},
        radius = 130,

        background_color = 0xFFFFFF,
        background_alpha = 0.7,
        background_thickness = 1,

        critical_threshold = 20.,

        start_angle = 149,
        end_angle = 293,

        background_color_critical = 0xFFFFFF,
        -- background_alpha_critical = 0.2,
        background_alpha_critical = 0.7,
        background_thickness_critical = 1,

        bar_color = 0xFA002E,
        bar_alpha = 0.7,
        bar_thickness = 5,

        background_color = 0xFA002E,
        background_alpha = 0.5,
        background_thickness = 2,

        bar_color_critical = 0xFFFFFF,
        bar_alpha_critical = 0.7,
        bar_thickness_critical = 5,
    },

    {
        kind = 'ring_graph',
        conky_value = 'mpd_percent',
        center = {x = 210, y = 250},
        radius = 160,

        critical_threshold = 101.,

        background_color = 0xFFFFFF,
        background_alpha = 0.7,
        background_thickness = 0,

        bar_color = 0xFFFFFF,
        bar_alpha = 0.7,
        bar_thickness = 6,

        start_angle = 155,
        end_angle = 288,

        change_color_on_critical = false,
        change_alpha_on_critical = false,
        change_thickness_on_critical = false,
    },
}
