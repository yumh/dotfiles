#!/bin/sh

ln -sv `pwd`/herbstluftwm/ ~/.config/herbstluftwm
ln -sv `pwd`/dzen2/ ~/.config/dzen2
ln -sv `pwd`/xmobarrc ~/.xmobarrc
ln -sv `pwd`/conky_dzen ~/.config/conky_dzen
ln -sv `pwd`/xinitrc ~/.xinitrc
ln -sv `pwd`/compton.conf ~/.config/compton.conf
ln -sv `pwd`/conky ~/.config/conky
ln -sv `pwd`/i3blocks.conf ~/.i3blocks.conf
ln -sv `pwd`/i3gaps/ ~/.config/i3
ln -sv `pwd`/xprofile ~/.xprofile
ln -sv `pwd`/Xresources ~/.Xresources
ln -sv `pwd`/redshift.conf ~/.config/redshift.conf
ln -sv `pwd`/xmonad ~/.xmonad
ln -sv `pwd`/polybar ~/.config/polybar
ln -sv `pwd`/dunst/ ~/.config/dunst
ln -sv `pwd`/termite/ ~/.config/termite
