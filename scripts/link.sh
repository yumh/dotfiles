#!/bin/sh

mkdir -p ~/.local/bin/

ln -sv `pwd`/my-ip ~/.local/bin/my-ip
ln -sv `pwd`/battery ~/.local/bin/battery
ln -sv `pwd`/telegram-desktop ~/.local/bin/telegram-desktop
ln -sv `pwd`/term ~/.local/bin/term
ln -sv `pwd`/dzen-vol ~/.local/bin/dzen-vol
ln -sv `pwd`/start-conky ~/.local/bin/start-conky
ln -sv `pwd`/volume ~/.local/bin/volume
ln -sv `pwd`/music-out ~/.local/bin/music-out
ln -sv `pwd`/tmux-right-side ~/.local/bin/tmux-right-side
ln -sv `pwd`/shared ~/.local/bin/shared
ln -sv `pwd`/update-pkgs ~/.local/bin/update-pkgs
ln -sv `pwd`/mail-counter ~/.local/bin/mail-counter
ln -sv `pwd`/messages ~/.local/bin/messages
ln -sv `pwd`/get-mpd-thumb ~/.local/bin/get-mpd-thumb
ln -sv `pwd`/mpd-notify ~/.local/bin/mpd-notify
