set -x PATH ~/.node_modules/bin ~/.gem/ruby/2.3/bin/ ~/.cabal/bin/ ~/.local/bin ~/.stack/programs/x86_64-freebsd/ghc-8.0.2/bin $PATH
set -x PAGER "less"
set -x MANPAGER "less"
set -x LESS "-iMr"
set -x VISUAL "vim"
set -x EDITOR "vim"

# gpg
set -x GPG_TTY (tty)

# term is a custom script in that fire up urxvtc and auto-start the deamon
set -x TERMINAL "term"

set -x MOZ_USE_OMTC 1

alias v "vim"
alias e "emacs -nw"
alias e "emacsclient -t -nw"

# git alias
alias gst "git status"
alias ga "git add"
alias gc "git commit"
alias gco "git checkout"
alias gm "git merge"

#alias _ "sudo"
#alias __ "sudo -E"

set fish_color_cwd 'red'
set fish_color_user 'red'
set fish_color_host 'magenta'

